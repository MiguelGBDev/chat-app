## Proposal

Implement a community channel, where you can connect not only with many fans around the world but also directly with your idol.
This Channel can be modeled much like a Chat room, and as we are an API first company,
we would like to see how you would model a small API to model a chat like behavior with several endpoints:

- Create a room
- Add a user to a room
- Send a message to a room
- Get latest messages from a room

## State of art

The proposal is implemented using some modern and advanced technologies to face the real problem.

- There are two Bounded Contexts for the requirements: RoomManagement and Messaging features.
- Every request needs the user ID on header for verification
- The project architecture follows Domain Driven Design (DDD) approach
- Every input parameter is validated and managed.
- Almost full test coverage.
- Containerized project, API and DB.
- OpenAPI documentation included.

![IMAGE_DESCRIPTION](./pictures/application.png)

## Description

## Technologies applied

- [PostgreSQL](https://www.postgresql.org/)
- [Docker](https://www.docker.com/) & [Docker desktop](https://www.docker.com/products/docker-desktop/)
- [Prisma ORM](https://www.prisma.io/)
- [Nodejs](https://nodejs.org/en)
- [Nestjs framework](https://github.com/nestjs/nest)
- [Postman](https://www.postman.com/)
- [DBeaver](https://dbeaver.io/)
- [Typescript](https://www.typescriptlang.org/)
- [Swagger](https://swagger.io/)

### Nest features

- Modules
- Services
- Controllers
- ScheduleModule

### Prisma features

- Migrations (Create the full structure of the database in an iterative way)
- Seeds (Populate the database with initial data)

### Why Nestjs?

Nestjs framework provides highly advanced features and design patterns in an easy way.
It provides a handy way to use dependency injection with a container design pattern.
It uses decorator features.

The main reason why I choose Nest to make this project is I consider it a good opportunity to learn about it and test if it speedup up the developing (IMHO, it does!).

### Why Prisma?

Prisma ORM provides protection to SQL Injection, and an easy middleware to make queries in a programmatic way.
It provides as well a useful database seed.
Prisma also includes migrations feature to create/modify the final database structure in an incremental mode, a very handy way to recover from errors in structure definition.

## Tech implementation

### Infrastructure

There are two Docker containers: API and Postgres Database.

The API Docker provided is ready to PROD, so changes on the API won't show changes without a new deployment.
It opens the PORT 3000 as an entrypoint to the docker container, making a port forwarding 3000 to 3000 port for the API.

The Database Docker provided includes a volume to store data permanently.
It opens the PORT 5432 as an entrypoint to the docker container, making a port forwarding 5432 to 5432 port for the API.

The connection between them is implemented with an internal net where the services see each other, as it is shared by them.

![IMAGE_DESCRIPTION](./pictures/Infrastructure.png)

### Database structure

We have three entities: Room, User, and Message.

- Message relates to Room and User.
- Relationship between Room and User is many to many, so its required an auxiliary table to relate them, RoomUserJoin.
- Message has a double foreign key to RoomUserJoin (roomId, userId). They allow us to verify the user is included in the room that is trying to send a new message.
  If not, database will throw an error (but it is controlled by the API as well)

![IMAGE_DESCRIPTION](./pictures/database.png)

### API arquitecture

The architecture of the API follows the DDD concepts for domain entities, use cases and business logic isolated.
There is an implementation for the design pattern repository to create an extra layer to communicate with the database to ingest or receive data.

![IMAGE_DESCRIPTION](./pictures/arquitecture.png)

## Explanations and comments

### Middlewares

There is one middleware that checks if every request is valid.

The middleware checks:

- If the request included the identifier of the user.
- If the user identifier has a valid UUID format.
- If the user identifier does exist in the database.

If all the cases are accomplished, the user is allowed to call the use cases.

### Use cases

#### Create a room

This use case creates a room on the database table Room, generating a new roomId for it.
After that, the room's owner is included on RoomUserJoin table.

The use case checks first if there is some room with the name passed as parameter. If it does, it returns an error.

#### Add user to a room

This use case creates a new relationship between a user and a room.
The user can be added to a room by himself, or by the room's owner.

This use case expects three parameters:

- The applicationId (The header userId of the user that makes the request).
- The identifier of the user to add to the room.
- The room where the user will be added.

The algorithm checks if every value exists on DB first.
Then, checks if the user who made the requests is the owner of the room, or if he is the same as the user to add.
If not, the request with be invalidated.
Then, checks if the user is already on the room, if not, invalidated request.

If every constraint is accomplish, the user will be added to the room, creating a new row on RoomUserJoin table.

#### Send a message to a room

This use case creates a new message on the database table.
The message needs several parameters:

- The text of the message is mandatory.
- The message should be sent by a user.
- The message should be related to a room.

Constraints:

- The user that sent the message should be added to the room previously.

#### Get latest messages of a room

To accomplish this feature, I implemented a pagination mechanism that responses with the last messages depending of the pagination parameters.
By default, with no pagination parameters, the use case returns the messages ordered by last date and autoincrement (I created this column to sort correctly messages inserted at the same moment).

### Seed script

The initial data ingest some users, rooms, messages, and relationships between them.
The data have statics identifiers.
There is one Postman collection with predefined requests. As the identifiers are generated statically, there is no need to modify those postman requests.

## Requirements

- npm
- nodejs
- docker

Open a terminal and type:

```bash
$ npm install
```

With this command all interal dependencies for the project will be added.

## Prepare the environment

### Build the infrastructure, launch full project

First, we will deploy two docker containers with docker-compose:

```bash
$ nmp run infra:up
```

This command will deploy a container for the API, and another one for the database.
It also provides a volume for data persistency and a internat net to communitate both containers.

### Create database structure and insert initial data

The second and the third step is to create the data structure and fill the DB with the initial mocked data. We will accomplish it with a single command:

```bash
$ npm run db:reset
```

** CAUTION: If you ran it previously and you use it another time, previous data will be removed! **

## Running the app

The project is ready now. Everything were deployed with
`npm run infra:up` command (API included). No need for extra steps.

If you want to turn off the full project, just use `npm run infra:down`.

** Please take note that the command above will turn off the database container as well **

### OpenAPI documentation

Once the app is fully deployed (see below the required steps), the API documentation is provided at:

`http://localhost:3000/api#/`

### Check results

Import the Postman collection include it in the source folder in a Postman client and send the predefined requests to check the responses.

** Please take note the Postman collection is prepared to expect predefined responses. If you use POST [OK] requests, the state of the DB will change and its possible to face unexpected behaviours. If you want to retry successfully some requests after those actions, please reset the database's initial data with `npm run db:reset` **

## Test

There is good coverage of unitary testing here. They include mocks, spies, and other Jest features.
I preferred to invest more time in the project arquitecture, so I didn't implement E2E or integration tests.

This test suite covers all cases shown on Postman collection and more in-deep issues.

To run the test suite:

```bash
$ npm test
```
