import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

async function main() {
  await prisma.user.createMany({
    data: [
      // id is passed as input to make it static. (This prevents to modify Postman requests collection)
      { id: '14b3f7ea-22ed-4e63-bc1d-9f50a6133af9', name: 'Bruce', surname: 'Springteen', alias: 'BruceGuitar' },
      { id: 'a0a6b56d-04dc-4a2d-95a8-7e6eead74312', name: 'Mike', surname: 'Oldfield', alias: 'Moonlight' },
      { id: 'cddbda0e-b709-4fb4-8b4e-8f9736405d30', name: 'Joaquin', surname: 'Sabina', alias: 'TanJovenYTanViejo' },
      { id: '80cc2550-81d4-4141-bc89-7bc7a519e4aa', name: 'Roberto', surname: 'Iniesta', alias: 'Evaristo' },
      { id: '61842d49-a67c-451c-9675-537b9eca963c', name: 'Robert', surname: 'Plant', alias: 'LedSinger' }
    ]
  })

  /**
   * Prisma createMany does not support 'RETURNING *' so I need to query for the userIds id
   * See more: https://github.com/prisma/prisma/issues/8131
   */
  const userIds = (await prisma.user.findMany()).map(elem => elem.id)

  await prisma.room.createMany({
    data: [
      {
        id: 'de47f7b3-527e-4f7d-a631-8ea0e366bc0f',
        name: 'Rock',
        description: 'Room to talk about our favourite music style',
        ownerId: userIds[2]
      },
      {
        id: 'd6944a43-3256-420a-9429-8d212bc6b6aa',
        name: 'Guitars',
        description: 'Which guitar do you prefer and why?',
        ownerId: userIds[0]
      },
      {
        id: '970f7067-4bb1-4fd8-babe-e419df90cf5d',
        name: 'Singers',
        description: 'Discussion about the best singer ever',
        ownerId: userIds[1]
      },
      {
        id: '0d66d7b8-9ce2-4327-abfd-0d46723881fc',
        name: 'Drums',
        description: 'Which drum brand do you prefer?',
        ownerId: userIds[3]
      }
    ]
  })

  const roomIds = (await prisma.room.findMany()).map(elem => elem.id)

  await prisma.roomUsersJoin.createMany({
    data: [
      { userId: userIds[0], roomId: roomIds[2] },
      { userId: userIds[0], roomId: roomIds[0] },
      { userId: userIds[0], roomId: roomIds[1] },
      { userId: userIds[1], roomId: roomIds[0] },
      { userId: userIds[1], roomId: roomIds[2] },
      { userId: userIds[2], roomId: roomIds[1] },
      { userId: userIds[2], roomId: roomIds[2] },
      { userId: userIds[3], roomId: roomIds[3] }
    ]
  })

  const data = [
    { text: 'I really like how sound As I lay dying', userId: userIds[0], roomId: roomIds[0] },
    { text: 'Yeah they are great, but Tool is so much better IMO', userId: userIds[1], roomId: roomIds[0] },
    { text: 'Could be, but they are different styles', userId: userIds[0], roomId: roomIds[0] },
    {
      text: 'Does anyone know what guitar bruce plays in tougher than the rest?',
      userId: userIds[2],
      roomId: roomIds[1]
    },
    { text: "Yeah, its me! it is an custom Fender telecaster '74", userId: userIds[0], roomId: roomIds[1] },
    { text: 'Thanks Bruce!', userId: userIds[2], roomId: roomIds[1] },
    { text: 'I would like to sing like In flames singer, he is amazing!', userId: userIds[3], roomId: roomIds[3] }
  ]
  for (let i = 0; i < 15; i++) data.push({ text: `${i}`, userId: userIds[2], roomId: roomIds[1] })

  await prisma.message.createMany({
    data
  })
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
