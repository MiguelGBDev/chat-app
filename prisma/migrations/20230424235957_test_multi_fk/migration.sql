-- DropForeignKey
ALTER TABLE "Message" DROP CONSTRAINT "Message_roomId_fkey";

-- DropForeignKey
ALTER TABLE "Message" DROP CONSTRAINT "Message_userId_fkey";

-- AddForeignKey
ALTER TABLE "Message" ADD CONSTRAINT "Message_userId_roomId_fkey" FOREIGN KEY ("userId", "roomId") REFERENCES "RoomUsersJoin"("userId", "roomId") ON DELETE RESTRICT ON UPDATE CASCADE;
