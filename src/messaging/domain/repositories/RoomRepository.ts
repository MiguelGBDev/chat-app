import { Room } from '../entities/room.entity'

export abstract class RoomRepository {
  abstract getById(roomId: string): Promise<Room>
}
