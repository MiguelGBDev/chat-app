import { Message } from '../entities/message.entity'

export abstract class MessageRepository {
  abstract create(message: Message): Promise<Message>
  abstract getByRoomId(roomId: string, skip?: number, take?: number): Promise<Message[]>
}
