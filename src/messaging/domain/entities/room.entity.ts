import { BadRequestException } from '@nestjs/common'

export class Room {
  constructor(public readonly id: string, public readonly ownerId: string) {
    if (!id) {
      throw new BadRequestException('Missing the room id')
    } else if (!ownerId) {
      throw new BadRequestException('Room needs an owner')
    }
  }
}
