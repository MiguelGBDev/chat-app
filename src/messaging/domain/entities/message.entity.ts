import { BadRequestException } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import * as uuid from 'uuid'

export class Message {
  @ApiProperty()
  public readonly roomId: string

  @ApiProperty()
  public readonly userId: string

  @ApiProperty()
  public readonly text: string

  @ApiProperty()
  public readonly messageId?: string

  constructor(roomId: string, userId: string, text: string, messageId?: string) {
    if (!roomId) throw new BadRequestException('Missing room')
    if (!userId) throw new BadRequestException('Missing user')
    if (!text) throw new BadRequestException('Missing text')
    if (!uuid.validate(roomId)) throw new BadRequestException(`The room id ${roomId} is not valid`)
    if (!uuid.validate(userId)) throw new BadRequestException(`The user id ${userId} is not valid`)

    this.roomId = roomId
    this.userId = userId
    this.text = text
    this.messageId = messageId
  }
}
