import { Test, TestingModule } from '@nestjs/testing'
import { createMock } from '@golevelup/ts-jest'
import * as uuid from 'uuid'

import { MessageController } from './message.controller'
import { SendMessageToRoomUseCase } from '../../application/useCases/sendMessageToRoom.useCase'
import { NewMessageRequest } from './request/newMessageRequest'
import { Message } from '../../domain/entities/message.entity'
import { GetRoomLastMessagesUseCase } from '../../application/useCases/getRoomLastMessages.useCase'

jest.mock('uuid')

describe('MessageController', () => {
  let spyValidate: jest.SpyInstance
  let spyCreate: jest.SpyInstance
  let controller: MessageController
  let sendMessageToRoomUseCase: SendMessageToRoomUseCase

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MessageController],
      providers: [
        { provide: SendMessageToRoomUseCase, useValue: createMock<SendMessageToRoomUseCase>() },
        { provide: GetRoomLastMessagesUseCase, useValue: createMock<GetRoomLastMessagesUseCase>() }
      ]
    }).compile()

    controller = module.get<MessageController>(MessageController)
    sendMessageToRoomUseCase = module.get<SendMessageToRoomUseCase>(SendMessageToRoomUseCase)
    spyValidate = jest.spyOn(uuid, 'validate').mockReturnValue(true)
    spyCreate = sendMessageToRoomUseCase.execute as jest.Mock
  })

  afterEach(() => {
    spyValidate.mockClear()
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  describe('POST createMessage', () => {
    const ROOM_ID = 'roomId'
    const USER_ID = 'userId'
    const MESSAGE_ID = 'messageId'
    const CREATE_MESSAGE_REQUEST: NewMessageRequest = { text: 'text message' }
    const USER_HEADER = { 'user-id': USER_ID }

    it('should return the new message', async () => {
      const MESSAGE = new Message(ROOM_ID, USER_ID, CREATE_MESSAGE_REQUEST.text)
      const CREATED_MESSAGE = new Message(ROOM_ID, USER_ID, CREATE_MESSAGE_REQUEST.text, MESSAGE_ID)
      spyCreate.mockReturnValue(CREATED_MESSAGE)

      const response = await controller.createMessage(
        USER_HEADER as unknown as Headers,
        ROOM_ID,
        CREATE_MESSAGE_REQUEST
      )

      expect(spyValidate).toHaveBeenCalled()
      expect(response).toStrictEqual(CREATED_MESSAGE)
      expect(spyCreate).toHaveBeenCalledWith(MESSAGE)
    })

    it('should throw an error if room id missing', async () => {
      const MISSING_ROOM_ERROR = 'Missing room'

      await expect(async () =>
        controller.createMessage(USER_HEADER as unknown as Headers, undefined, CREATE_MESSAGE_REQUEST)
      ).rejects.toThrowError(MISSING_ROOM_ERROR)
      expect(spyCreate).not.toHaveBeenCalled()
    })

    it('should throw an error if user id missing', async () => {
      const WRONG_USER_HEADER = { 'user-if': undefined }
      const MISSING_USER_ERROR = 'Missing user'

      await expect(async () =>
        controller.createMessage(WRONG_USER_HEADER as unknown as Headers, ROOM_ID, CREATE_MESSAGE_REQUEST)
      ).rejects.toThrowError(MISSING_USER_ERROR)
      expect(spyCreate).not.toHaveBeenCalled()
    })

    it('should throw an error if text missing', async () => {
      const WRONG_CREATE_MESSAGE_REQUEST = {}
      const MISSING_TEXT_ERROR = 'Missing text'

      await expect(async () =>
        controller.createMessage(
          USER_HEADER as unknown as Headers,
          ROOM_ID,
          WRONG_CREATE_MESSAGE_REQUEST as NewMessageRequest
        )
      ).rejects.toThrowError(MISSING_TEXT_ERROR)
      expect(spyCreate).not.toHaveBeenCalled()
    })

    it('should throw an error if room id is not an UUID', async () => {
      const NOT_UUID_ROOM_ID = 'randomValue'
      spyValidate.mockReturnValue(false)
      const ROOM_ID_NOT_UUID_ERROR = `The room id ${NOT_UUID_ROOM_ID} is not valid`
      await expect(async () =>
        controller.createMessage(USER_HEADER as unknown as Headers, NOT_UUID_ROOM_ID, CREATE_MESSAGE_REQUEST)
      ).rejects.toThrowError(ROOM_ID_NOT_UUID_ERROR)
      expect(spyValidate).toHaveBeenCalledTimes(1)
      expect(spyCreate).not.toHaveBeenCalled()
    })

    it('should throw an error if user id is not an UUID', async () => {
      const NOT_UUID_USER_ID = 'randomValue'
      const WRONG_USER_HEADER = { 'user-id': NOT_UUID_USER_ID }
      spyValidate.mockReturnValueOnce(true).mockReturnValue(false)
      const USER_ID_NOT_UUID_ERROR = `The user id ${NOT_UUID_USER_ID} is not valid`
      await expect(async () =>
        controller.createMessage(WRONG_USER_HEADER as unknown as Headers, ROOM_ID, CREATE_MESSAGE_REQUEST)
      ).rejects.toThrowError(USER_ID_NOT_UUID_ERROR)
      expect(spyValidate).toHaveBeenCalledTimes(2)
      expect(spyCreate).not.toHaveBeenCalled()
    })
  })
})
