import { Type } from 'class-transformer'
import { IsInt, IsOptional } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class GetMessagesRequest {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsInt()
  @Type(() => Number)
  skip: number

  @ApiProperty({ required: false })
  @IsOptional()
  @IsInt()
  @Type(() => Number)
  take: number
}
