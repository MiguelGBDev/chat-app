import { ApiProperty } from '@nestjs/swagger'

export class NewMessageRequest {
  @ApiProperty()
  text: string
}
