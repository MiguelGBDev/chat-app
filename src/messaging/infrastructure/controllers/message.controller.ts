import {
  Controller,
  Post,
  Body,
  Param,
  Headers,
  Get,
  BadRequestException,
  Query,
  ValidationPipe,
  UsePipes
} from '@nestjs/common'
import { ApiCreatedResponse, ApiHeader, ApiTags } from '@nestjs/swagger'

import { NewMessageRequest } from './request/newMessageRequest'
import { GetMessagesRequest } from './request/getMessagesRequest'
import { Message } from '../../domain/entities/message.entity'
import { SendMessageToRoomUseCase } from '../../application/useCases/sendMessageToRoom.useCase'
import { GetRoomLastMessagesUseCase } from '../../application/useCases/getRoomLastMessages.useCase'

@ApiTags('Messaging')
@Controller('messaging/rooms')
export class MessageController {
  constructor(
    private readonly sendMessageToRoomUseCase: SendMessageToRoomUseCase,
    private readonly getRoomLastMessagesUseCase: GetRoomLastMessagesUseCase
  ) {}

  @ApiHeader({
    name: 'user-id',
    description: 'User identifier'
  })
  @Post('/:roomId/messages')
  @ApiCreatedResponse({ type: Message })
  createMessage(
    @Headers() headers: Headers,
    @Param('roomId') roomId: string,
    @Body() messageRequest: NewMessageRequest
  ) {
    const userId = headers['user-id']
    const { text } = messageRequest

    const message = new Message(roomId, userId, text)

    return this.sendMessageToRoomUseCase.execute(message)
  }

  @ApiHeader({
    name: 'user-id',
    description: 'User identifier'
  })
  @ApiCreatedResponse({ type: Message, isArray: true })
  @Get('/:roomId/messages')
  @UsePipes(new ValidationPipe({ transform: true }))
  getMessages(@Headers() headers: Headers, @Param('roomId') roomId: string, @Query() queryParams: GetMessagesRequest) {
    const userId = headers['user-id']
    const { skip, take } = queryParams
    if (!roomId) throw new BadRequestException('Missing the room id')

    if (!!skip !== !!take) throw new BadRequestException('Wrong pagination parameters')
    return this.getRoomLastMessagesUseCase.execute(roomId, userId, skip, take)
  }
}
