import { Test, TestingModule } from '@nestjs/testing'
import { PrismaClient } from '@prisma/client'
import { createMock } from '@golevelup/ts-jest'

import { PrismaRoomRepository } from './prisma.room.repository'
import { Room } from '../../domain/entities/room.entity'

const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const ROOM_NAME = 'newRoom'
const ROOM_DB = { id: ROOM_ID, name: ROOM_NAME, ownerId: USER_ID, description: null }
const RETURNED_ROOM = new Room(ROOM_ID, USER_ID)

describe('PrismaRoomRepository', () => {
  let repository: PrismaRoomRepository
  let prismaClientMock: PrismaClient

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrismaRoomRepository, { provide: PrismaClient, useValue: createMock<PrismaClient>() }]
    }).compile()

    repository = module.get<PrismaRoomRepository>(PrismaRoomRepository)
    prismaClientMock = module.get<PrismaClient>(PrismaClient)
  })

  it('should be defined', () => {
    expect(repository).toBeDefined()
  })

  describe('getById', () => {
    it('should return the room', async () => {
      prismaClientMock.room.findUnique = jest.fn().mockReturnValue(ROOM_DB)

      const response = await repository.getById(USER_ID)

      expect(response).toStrictEqual(RETURNED_ROOM)
      expect(prismaClientMock.room.findUnique).toHaveBeenCalled()
    })

    it('should return undefined if the room is not found', async () => {
      prismaClientMock.room.findUnique = jest.fn().mockReturnValue(undefined)

      const response = await repository.getById(RETURNED_ROOM.id)

      expect(response).toStrictEqual(undefined)
      expect(prismaClientMock.room.findUnique).toHaveBeenCalled()
    })
  })
})
