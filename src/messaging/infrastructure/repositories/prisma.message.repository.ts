import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { MessageRepository } from '../..//domain/repositories/MessageRepository'
import { Message } from '../../domain/entities/message.entity'

@Injectable()
export class PrismaMessageRepository implements MessageRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async create(message: Message): Promise<Message> {
    const { text, userId, roomId } = message
    const response = await this.prisma.message.create({
      data: { text, userId, roomId }
    })

    return new Message(response.roomId, response.userId, response.text, response.id)
  }

  async getByRoomId(roomId: string, skip?: number, take?: number): Promise<Message[]> {
    const response = await this.prisma.message.findMany({
      where: { roomId },
      skip,
      take,
      orderBy: [{ createdAt: 'desc' }, { counter: 'desc' }]
    })

    if (!response.length) return []
    else {
      const messages: Message[] = []
      for (const message of response) {
        const { text, userId, roomId, id } = message
        messages.push(new Message(roomId, userId, text, id))
      }
      return messages
    }
  }
}
