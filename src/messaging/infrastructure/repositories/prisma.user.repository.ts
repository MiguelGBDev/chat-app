import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { UserRepository } from '../../domain/repositories/UserRepository'
import { User } from '../../domain/entities/user.entity'

@Injectable()
export class PrismaUserRepository implements UserRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async getById(id: string): Promise<User> {
    const userDb = await this.prisma.user.findUnique({ where: { id }, include: { rooms: true } })

    if (userDb) {
      const roomIds = userDb.rooms?.map(elem => elem.roomId)
      return new User(userDb.id, roomIds)
    } else return undefined
  }
}
