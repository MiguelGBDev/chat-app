import { Test, TestingModule } from '@nestjs/testing'
import { PrismaClient } from '@prisma/client'
import { createMock } from '@golevelup/ts-jest'
import { PrismaUserRepository } from './prisma.user.repository'
import { User } from '../../domain/entities/user.entity'

const USER_ID = 'userId'
const USER_DB = { id: USER_ID, rooms: [] }
const RETURNED_USER = new User(USER_ID, [])

describe('PrismaUserRepository', () => {
  let repository: PrismaUserRepository
  let prismaClientMock: PrismaClient

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrismaUserRepository, { provide: PrismaClient, useValue: createMock<PrismaClient>() }]
    }).compile()

    repository = module.get<PrismaUserRepository>(PrismaUserRepository)
    prismaClientMock = module.get<PrismaClient>(PrismaClient)
  })

  it('should be defined', () => {
    expect(repository).toBeDefined()
  })

  describe('getById', () => {
    it('should return the user', async () => {
      prismaClientMock.user.findUnique = jest.fn().mockReturnValue(USER_DB)

      const response = await repository.getById(USER_ID)

      expect(response).toStrictEqual(RETURNED_USER)
      expect(prismaClientMock.user.findUnique).toHaveBeenCalled()
    })

    it('should return undefined if the user is not found', async () => {
      prismaClientMock.user.findUnique = jest.fn().mockReturnValue(undefined)

      const response = await repository.getById(USER_ID)

      expect(response).toStrictEqual(undefined)
      expect(prismaClientMock.user.findUnique).toHaveBeenCalled()
    })
  })
})
