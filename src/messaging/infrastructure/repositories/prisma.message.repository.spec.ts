import { Test, TestingModule } from '@nestjs/testing'
import { PrismaClient } from '@prisma/client'
import { createMock } from '@golevelup/ts-jest'
import * as uuid from 'uuid'

import { PrismaMessageRepository } from './prisma.message.repository'
import { Message } from '../../domain/entities/message.entity'

jest.mock('uuid')

const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const MESSAGE_ID = 'messageId'
const MESSAGE_TEXT = 'newMessage'
const MESSAGE_DB = { id: MESSAGE_ID, roomId: ROOM_ID, userId: USER_ID, text: MESSAGE_TEXT }

describe('PrismaMessageRepository', () => {
  let spyValidate: jest.SpyInstance
  let repository: PrismaMessageRepository
  let prismaClientMock: PrismaClient
  let INPUT_MESSAGE: Message
  let OUTPUT_MESSAGE: Message

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrismaMessageRepository, { provide: PrismaClient, useValue: createMock<PrismaClient>() }]
    }).compile()

    repository = module.get<PrismaMessageRepository>(PrismaMessageRepository)
    prismaClientMock = module.get<PrismaClient>(PrismaClient)

    spyValidate = jest.spyOn(uuid, 'validate').mockReturnValue(true)

    INPUT_MESSAGE = new Message(ROOM_ID, USER_ID, MESSAGE_TEXT)
    OUTPUT_MESSAGE = new Message(ROOM_ID, USER_ID, MESSAGE_TEXT, MESSAGE_ID)
  })

  afterEach(() => {
    spyValidate.mockClear()
  })

  it('should be defined', () => {
    expect(repository).toBeDefined()
  })

  describe('create', () => {
    it('should return the new message', async () => {
      prismaClientMock.message.create = jest.fn().mockReturnValue(MESSAGE_DB)

      const response = await repository.create(INPUT_MESSAGE)

      expect(response).toStrictEqual(OUTPUT_MESSAGE)
      expect(prismaClientMock.message.create).toHaveBeenCalled()
    })

    it('should throw error if already exist a message with the same name', async () => {
      prismaClientMock.message.create = jest.fn().mockRejectedValue(new Error())

      await expect(async () => repository.create(INPUT_MESSAGE)).rejects.toThrowError()
      expect(prismaClientMock.message.create).toHaveBeenCalled()
    })
  })

  describe('getByRoomId', () => {
    const USER_ID_2 = 'userId'

    const MESSAGE_ID_2 = 'messageId'
    const MESSAGE_TEXT_2 = 'newMessage'
    let OUTPUT_MESSAGE_2: Message
    let OUTPUT_MESSAGE_ARRAY: Message[]
    const MESSAGE_DB_2 = { id: MESSAGE_ID, roomId: ROOM_ID, userId: USER_ID_2, text: MESSAGE_TEXT }

    beforeEach(async () => {
      OUTPUT_MESSAGE_2 = new Message(ROOM_ID, USER_ID_2, MESSAGE_TEXT_2, MESSAGE_ID_2)
      OUTPUT_MESSAGE_ARRAY = [OUTPUT_MESSAGE, OUTPUT_MESSAGE_2]
    })

    it('should return an array of the room messages', async () => {
      prismaClientMock.message.findMany = jest.fn().mockReturnValue([MESSAGE_DB, MESSAGE_DB_2])

      const response = await repository.getByRoomId(ROOM_ID)

      expect(response).toStrictEqual(OUTPUT_MESSAGE_ARRAY)
      expect(prismaClientMock.message.findMany).toHaveBeenCalled()
    })

    it('should return empty array if the list is empty in db', async () => {
      prismaClientMock.message.findMany = jest.fn().mockReturnValue([])

      const response = await repository.getByRoomId(ROOM_ID)

      expect(response).toStrictEqual([])
      expect(prismaClientMock.message.findMany).toHaveBeenCalled()
    })
  })
})
