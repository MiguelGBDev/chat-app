import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { Room } from '../../domain/entities/room.entity'
import { RoomRepository } from '../../domain/repositories/RoomRepository'

@Injectable()
export class PrismaRoomRepository implements RoomRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async getById(roomId: string): Promise<Room> {
    const response = await this.prisma.room.findUnique({ where: { id: roomId } })
    return response ? new Room(response.id, response.ownerId) : undefined
  }
}
