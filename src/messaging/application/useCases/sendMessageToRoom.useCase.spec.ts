import { Test, TestingModule } from '@nestjs/testing'
import { createMock } from '@golevelup/ts-jest'
import * as uuid from 'uuid'

import { SendMessageToRoomUseCase } from './sendMessageToRoom.useCase'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { UserRepository } from '../../domain/repositories/UserRepository'
import { MessageRepository } from '../../domain/repositories/MessageRepository'
import { Message } from '../../domain/entities/message.entity'

jest.mock('uuid')

const MESSAGE_ID = 'messageId'
const MESSAGE_TEXT = 'newMessage'
const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const ROOM_RESPONSE = {}

describe('SendMessageToRoomUseCase', () => {
  let useCase: SendMessageToRoomUseCase
  let roomRepository: RoomRepository
  let userRepository: UserRepository
  let messageRepository: MessageRepository

  let spyRoomGetById: jest.SpyInstance
  let spyUserGetById: jest.SpyInstance
  let spyMessageCreate: jest.SpyInstance

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SendMessageToRoomUseCase,
        { provide: RoomRepository, useValue: createMock<RoomRepository>() },
        { provide: UserRepository, useValue: createMock<UserRepository>() },
        { provide: MessageRepository, useValue: createMock<MessageRepository>() }
      ]
    }).compile()

    useCase = module.get<SendMessageToRoomUseCase>(SendMessageToRoomUseCase)
    roomRepository = module.get<RoomRepository>(RoomRepository)
    userRepository = module.get<UserRepository>(UserRepository)
    messageRepository = module.get<MessageRepository>(MessageRepository)

    jest.spyOn(uuid, 'validate').mockReturnValue(true)
    spyRoomGetById = roomRepository.getById as jest.Mock
    spyUserGetById = userRepository.getById as jest.Mock
    spyMessageCreate = messageRepository.create as jest.Mock
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should be defined', () => {
    expect(useCase).toBeDefined()
  })

  describe('execute', () => {
    const USER_RESPONSE = { id: USER_ID, rooms: [ROOM_ID] }
    const MESSAGE = { roomId: ROOM_ID, userId: USER_ID, text: MESSAGE_TEXT }
    const MESSAGE_RESPONSE = { roomId: ROOM_ID, userId: USER_ID, text: MESSAGE_TEXT, id: MESSAGE_ID }

    it(`user should send message to room`, async () => {
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(USER_RESPONSE)
      spyMessageCreate.mockReturnValue(MESSAGE_RESPONSE)

      const response = await useCase.execute(new Message(ROOM_ID, USER_ID, MESSAGE_TEXT))

      expect(response).toBe(MESSAGE_RESPONSE)
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageCreate).toHaveBeenCalledWith(MESSAGE)
    })

    it(`should throw an error if the room id is not found`, async () => {
      const WRONG_ROOM_ID = 'wrongId'
      spyRoomGetById.mockReturnValue(undefined)
      spyUserGetById.mockReturnValue(USER_RESPONSE)
      spyMessageCreate.mockReturnValue(MESSAGE_RESPONSE)

      await expect(useCase.execute(new Message(WRONG_ROOM_ID, USER_ID, MESSAGE_TEXT))).rejects.toThrowError(
        `Room with id ${WRONG_ROOM_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(WRONG_ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageCreate).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user id is not found`, async () => {
      const WRONG_USER_ID = 'wrongId'
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(undefined)
      spyMessageCreate.mockReturnValue(MESSAGE_RESPONSE)

      await expect(useCase.execute(new Message(ROOM_ID, WRONG_USER_ID, MESSAGE_TEXT))).rejects.toThrowError(
        `User with id ${WRONG_USER_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(WRONG_USER_ID)
      expect(spyMessageCreate).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user is not registered in any room`, async () => {
      const USER_NO_ROOMS_RESPONSE = { id: USER_ID, rooms: [] }
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(USER_NO_ROOMS_RESPONSE)
      spyMessageCreate.mockReturnValue(MESSAGE_RESPONSE)

      await expect(useCase.execute(new Message(ROOM_ID, USER_ID, MESSAGE_TEXT))).rejects.toThrowError(
        'The user does not belong to the room'
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageCreate).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user is not registered in the room`, async () => {
      const ANOTHER_ROOM_ID = 'anotherRoomId'
      const USER_NOT_IN_ROOMS_RESPONSE = { id: USER_ID, rooms: [ANOTHER_ROOM_ID] }
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(USER_NOT_IN_ROOMS_RESPONSE)
      spyMessageCreate.mockReturnValue(MESSAGE_RESPONSE)

      await expect(useCase.execute(new Message(ROOM_ID, USER_ID, MESSAGE_TEXT))).rejects.toThrowError(
        'The user does not belong to the room'
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageCreate).not.toHaveBeenCalled()
    })
  })
})
