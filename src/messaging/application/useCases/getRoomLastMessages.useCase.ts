import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common'

import { Message } from '../../domain/entities/message.entity'
import { MessageRepository } from '../../domain/repositories/MessageRepository'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { UserRepository } from '../../domain/repositories/UserRepository'

@Injectable()
export class GetRoomLastMessagesUseCase {
  constructor(
    private readonly messageRepository: MessageRepository,
    private readonly userRepository: UserRepository,
    private readonly roomRepository: RoomRepository
  ) {}

  async execute(roomId: string, userId: string, skip?: number, take?: number): Promise<Message[]> {
    const [room, user] = await Promise.all([this.roomRepository.getById(roomId), this.userRepository.getById(userId)])
    if (!room) throw new NotFoundException(`Room with id ${roomId} not found`)
    if (!user) throw new NotFoundException(`User with id ${userId} not found`)

    if (!user.rooms?.includes(roomId)) throw new ForbiddenException('The user does not belong to the room')

    return await this.messageRepository.getByRoomId(roomId, skip, take)
  }
}
