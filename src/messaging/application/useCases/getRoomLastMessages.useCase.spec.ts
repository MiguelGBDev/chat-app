import { Test, TestingModule } from '@nestjs/testing'
import { createMock } from '@golevelup/ts-jest'
import * as uuid from 'uuid'

import { GetRoomLastMessagesUseCase } from './getRoomLastMessages.useCase'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { UserRepository } from '../../domain/repositories/UserRepository'
import { MessageRepository } from '../../domain/repositories/MessageRepository'
import { Message } from '../../domain/entities/message.entity'

jest.mock('uuid')

const MESSAGE_ID = 'messageId'
const MESSAGE_TEXT = 'newMessage'
const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const ROOM_RESPONSE = {}

describe('GetRoomLastMessagesUseCase', () => {
  let useCase: GetRoomLastMessagesUseCase
  let roomRepository: RoomRepository
  let userRepository: UserRepository
  let messageRepository: MessageRepository

  let spyRoomGetById: jest.SpyInstance
  let spyUserGetById: jest.SpyInstance
  let spyMessageGetByRoomId: jest.SpyInstance

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GetRoomLastMessagesUseCase,
        { provide: RoomRepository, useValue: createMock<RoomRepository>() },
        { provide: UserRepository, useValue: createMock<UserRepository>() },
        { provide: MessageRepository, useValue: createMock<MessageRepository>() }
      ]
    }).compile()

    useCase = module.get<GetRoomLastMessagesUseCase>(GetRoomLastMessagesUseCase)
    roomRepository = module.get<RoomRepository>(RoomRepository)
    userRepository = module.get<UserRepository>(UserRepository)
    messageRepository = module.get<MessageRepository>(MessageRepository)

    jest.spyOn(uuid, 'validate').mockReturnValue(true)
    spyRoomGetById = roomRepository.getById as jest.Mock
    spyUserGetById = userRepository.getById as jest.Mock
    spyMessageGetByRoomId = messageRepository.getByRoomId as jest.Mock
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should be defined', () => {
    expect(useCase).toBeDefined()
  })

  describe('execute', () => {
    const MESSAGE_ID_2 = 'messageId'
    const MESSAGE_TEXT_2 = 'newMessage'
    const USER_ID_2 = 'userId'
    const USER_RESPONSE = { id: USER_ID, rooms: [ROOM_ID] }
    let ARRAY_MESSAGE_RETURN: Message[]

    beforeEach(async () => {
      ARRAY_MESSAGE_RETURN = [
        new Message(ROOM_ID, USER_ID, MESSAGE_TEXT, MESSAGE_ID),
        new Message(ROOM_ID, USER_ID_2, MESSAGE_TEXT_2, MESSAGE_ID_2)
      ]
    })

    it(`should return list of messages`, async () => {
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(USER_RESPONSE)
      spyMessageGetByRoomId.mockReturnValue(ARRAY_MESSAGE_RETURN)

      const response = await useCase.execute(ROOM_ID, USER_ID)

      expect(response).toBe(ARRAY_MESSAGE_RETURN)
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageGetByRoomId).toHaveBeenCalledWith(ROOM_ID, undefined, undefined)
    })

    it(`should throw an error if the room id is not found`, async () => {
      const WRONG_ROOM_ID = 'wrongId'
      spyRoomGetById.mockReturnValue(undefined)
      spyUserGetById.mockReturnValue(USER_RESPONSE)
      spyMessageGetByRoomId.mockReturnValue(ARRAY_MESSAGE_RETURN)

      await expect(useCase.execute(WRONG_ROOM_ID, USER_ID)).rejects.toThrowError(
        `Room with id ${WRONG_ROOM_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(WRONG_ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageGetByRoomId).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user id is not found`, async () => {
      const WRONG_USER_ID = 'wrongId'
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(undefined)
      spyMessageGetByRoomId.mockReturnValue(ARRAY_MESSAGE_RETURN)

      await expect(useCase.execute(ROOM_ID, WRONG_USER_ID)).rejects.toThrowError(
        `User with id ${WRONG_USER_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(WRONG_USER_ID)
      expect(spyMessageGetByRoomId).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user is not registered in any room`, async () => {
      const USER_NO_ROOMS_RESPONSE = { id: USER_ID, rooms: [] }
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(USER_NO_ROOMS_RESPONSE)
      spyMessageGetByRoomId.mockReturnValue(ARRAY_MESSAGE_RETURN)

      await expect(useCase.execute(ROOM_ID, USER_ID)).rejects.toThrowError('The user does not belong to the room')
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageGetByRoomId).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user is not registered in the room`, async () => {
      const ANOTHER_ROOM_ID = 'anotherRoomId'
      const USER_NOT_IN_ROOMS_RESPONSE = { id: USER_ID, rooms: [ANOTHER_ROOM_ID] }
      spyRoomGetById.mockReturnValue(ROOM_RESPONSE)
      spyUserGetById.mockReturnValue(USER_NOT_IN_ROOMS_RESPONSE)
      spyMessageGetByRoomId.mockReturnValue(ARRAY_MESSAGE_RETURN)

      await expect(useCase.execute(ROOM_ID, USER_ID)).rejects.toThrowError('The user does not belong to the room')
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenCalledWith(USER_ID)
      expect(spyMessageGetByRoomId).not.toHaveBeenCalled()
    })
  })
})
