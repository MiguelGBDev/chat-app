import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common'

import { Message } from '../../domain/entities/message.entity'
import { MessageRepository } from '../../domain/repositories/MessageRepository'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { UserRepository } from '../../domain/repositories/UserRepository'

@Injectable()
export class SendMessageToRoomUseCase {
  constructor(
    private readonly messageRepository: MessageRepository,
    private readonly userRepository: UserRepository,
    private readonly roomRepository: RoomRepository
  ) {}

  async execute(message: Message): Promise<Message> {
    const [room, user] = await Promise.all([
      this.roomRepository.getById(message.roomId),
      this.userRepository.getById(message.userId)
    ])
    if (!room) throw new NotFoundException(`Room with id ${message.roomId} not found`)
    if (!user) throw new NotFoundException(`User with id ${message.userId} not found`)

    if (!user.rooms?.includes(message.roomId)) throw new ForbiddenException('The user does not belong to the room')

    return await this.messageRepository.create(message)
  }
}
