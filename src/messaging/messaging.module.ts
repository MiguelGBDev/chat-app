import { Module } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { MessageController } from './infrastructure/controllers/message.controller'
import { MessageRepository } from './domain/repositories/MessageRepository'
import { RoomRepository } from './domain/repositories/RoomRepository'
import { UserRepository } from './domain/repositories/UserRepository'
import { PrismaMessageRepository } from './infrastructure/repositories/prisma.message.repository'
import { PrismaRoomRepository } from './infrastructure/repositories/prisma.room.repository'
import { PrismaUserRepository } from './infrastructure/repositories/prisma.user.repository'
import { SendMessageToRoomUseCase } from './application/useCases/sendMessageToRoom.useCase'
import { GetRoomLastMessagesUseCase } from './application/useCases/getRoomLastMessages.useCase'

@Module({
  controllers: [MessageController],
  providers: [
    SendMessageToRoomUseCase,
    GetRoomLastMessagesUseCase,
    { provide: RoomRepository, useClass: PrismaRoomRepository },
    { provide: UserRepository, useClass: PrismaUserRepository },
    { provide: MessageRepository, useClass: PrismaMessageRepository },
    PrismaClient
  ]
})
export class MessagingModule {}
