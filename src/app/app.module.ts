import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { getUserMiddleware } from './middlewares/getUser/getUser'
import { RoomManagementModule } from '../roomManagement/roomManagement.module'
import { MessagingModule } from '../messaging/messaging.module'

@Module({
  imports: [RoomManagementModule, MessagingModule],
  controllers: [],
  providers: [PrismaClient]
})
export class AppModule implements NestModule {
  async configure(consumer: MiddlewareConsumer) {
    consumer.apply(getUserMiddleware).forRoutes('*')
  }
}
