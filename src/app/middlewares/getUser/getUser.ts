import {
  BadRequestException,
  Injectable,
  NestMiddleware,
  NotFoundException,
  UnauthorizedException
} from '@nestjs/common'
import { PrismaClient } from '@prisma/client'
import { validate } from 'uuid'
import { Request, Response, NextFunction } from 'express'

@Injectable()
export class getUserMiddleware implements NestMiddleware {
  constructor(private readonly prisma: PrismaClient) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const userId = req.get('user-id')
    if (!userId) {
      throw new UnauthorizedException('Unable to receive user identifier')
    }

    if (!validate(userId)) {
      throw new BadRequestException(`The user id ${userId} is not valid`)
    }

    const user = await this.prisma.user.findUnique({
      where: { id: userId }
    })

    if (!user) {
      throw new NotFoundException('User not found')
    }

    next()
  }
}
