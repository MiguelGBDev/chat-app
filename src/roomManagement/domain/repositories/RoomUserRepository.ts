import { RoomUsersJoin } from '../entities/roomUsersJoin.entity'

export abstract class RoomUserRepository {
  abstract create(roomId: string, userId: string): Promise<RoomUsersJoin>
}
