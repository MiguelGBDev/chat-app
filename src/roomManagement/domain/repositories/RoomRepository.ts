import { Room } from '../entities/room.entity'

export abstract class RoomRepository {
  abstract create(room: Room): Promise<Room>
  abstract getById(roomId: string): Promise<Room>
  abstract getByName(name: string): Promise<Room>
}
