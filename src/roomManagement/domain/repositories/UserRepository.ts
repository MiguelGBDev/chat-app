import { User } from '../entities/user.entity'

export abstract class UserRepository {
  abstract getById(id: string): Promise<User>
}
