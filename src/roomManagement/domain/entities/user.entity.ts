export class User {
  constructor(public readonly id: string, public readonly rooms?: string[]) {
    if (!id) {
      throw new Error()
    }
  }
}
