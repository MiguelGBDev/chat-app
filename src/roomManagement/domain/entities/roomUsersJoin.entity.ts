import { ApiProperty } from '@nestjs/swagger'

export class RoomUsersJoin {
  @ApiProperty()
  public readonly userId: string

  @ApiProperty()
  public readonly roomId: string

  constructor(userId: string, roomId: string) {
    this.userId = userId
    this.roomId = roomId
  }
}
