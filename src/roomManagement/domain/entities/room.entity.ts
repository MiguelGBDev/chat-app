import { BadRequestException } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'

export class Room {
  @ApiProperty()
  public readonly name: string

  @ApiProperty()
  public readonly ownerId: string

  @ApiProperty({ required: false })
  public readonly description?: string

  @ApiProperty()
  public readonly id?: string

  constructor(name: string, ownerId: string, description?: string, id?: string) {
    if (!name) {
      throw new BadRequestException('Room needs a name')
    } else if (!ownerId) {
      throw new BadRequestException('Room needs an owner')
    }

    this.name = name
    this.ownerId = ownerId
    this.description = description
    this.id = id
  }
}
