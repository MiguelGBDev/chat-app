import { Module } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { AddUserToRoomUseCase } from './application/useCases/addUserToRoom.useCase'
import { CreateRoomUseCase } from './application/useCases/createRoom.useCase'
import { RoomController } from './infrastructure/controllers/room.controller'
import { RoomRepository } from './domain/repositories/RoomRepository'
import { UserRepository } from './domain/repositories/UserRepository'
import { RoomUserRepository } from './domain/repositories/RoomUserRepository'
import { PrismaRoomRepository } from './infrastructure/repositories/prisma.room.repository'
import { PrismaUserRepository } from './infrastructure/repositories/prisma.user.repository'
import { PrismaRoomUserRepository } from './infrastructure/repositories/prisma.roomUser.repository'

@Module({
  controllers: [RoomController],
  providers: [
    CreateRoomUseCase,
    AddUserToRoomUseCase,
    PrismaClient,
    { provide: RoomRepository, useClass: PrismaRoomRepository },
    { provide: UserRepository, useClass: PrismaUserRepository },
    { provide: RoomUserRepository, useClass: PrismaRoomUserRepository }
  ]
})
export class RoomManagementModule {}
