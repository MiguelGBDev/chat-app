import { Test, TestingModule } from '@nestjs/testing'
import { createMock } from '@golevelup/ts-jest'
import * as uuid from 'uuid'

import { RoomController } from './room.controller'
import { CreateRoomRequest } from './request/createRoomRequest'
import { AddUserToRoomRequest } from './request/addUserToRoomRequest'
import { AddUserToRoomUseCase } from '../../application/useCases/addUserToRoom.useCase'
import { CreateRoomUseCase } from '../../application/useCases/createRoom.useCase'
import { Room } from '../../domain/entities/room.entity'

jest.mock('uuid')

const USER_ID = 'userId'
const OWNER_ID = 'ownerId'
const USER_HEADER = { 'user-id': USER_ID }
const OWNER_HEADER = { 'user-id': OWNER_ID }

describe('RoomController', () => {
  const validateSpy = jest.spyOn(uuid, 'validate')
  let controller: RoomController
  let createRoomUseCase: CreateRoomUseCase
  let addUserToRoomUseCase: AddUserToRoomUseCase

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoomController],
      providers: [
        { provide: CreateRoomUseCase, useValue: createMock<CreateRoomUseCase>() },
        { provide: AddUserToRoomUseCase, useValue: createMock<AddUserToRoomUseCase>() }
      ]
    }).compile()

    controller = module.get<RoomController>(RoomController)
    createRoomUseCase = module.get<CreateRoomUseCase>(CreateRoomUseCase)
    addUserToRoomUseCase = module.get<AddUserToRoomUseCase>(AddUserToRoomUseCase)
  })

  afterEach(() => {
    validateSpy.mockClear()
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  describe('POST create', () => {
    const NAME_ROOM = 'newRoom'
    const CREATE_ROOM_REQUEST = { name: NAME_ROOM }
    const ROOM = new Room(NAME_ROOM, USER_ID)
    const CREATED_ROOM_MOCK = {}
    const WRONG_CREATE_ROOM_REQUEST = {}

    it('should return the new room', async () => {
      const spyCreate = (createRoomUseCase.execute as jest.Mock).mockReturnValue(CREATED_ROOM_MOCK)

      const response = await controller.create(USER_HEADER as unknown as Headers, CREATE_ROOM_REQUEST)

      expect(response).toStrictEqual(CREATED_ROOM_MOCK)
      expect(spyCreate).toHaveBeenCalledWith(ROOM)
    })

    it('should throw error if CreateRoomRequest is missing room name', async () => {
      const MISSING_NAME_ERROR = 'Room needs a name'
      const spyCreate = createRoomUseCase.execute as jest.Mock

      await expect(async () =>
        controller.create(USER_HEADER as unknown as Headers, WRONG_CREATE_ROOM_REQUEST as CreateRoomRequest)
      ).rejects.toThrowError(MISSING_NAME_ERROR)
      expect(spyCreate).not.toHaveBeenCalled()
    })

    it('should throw error if the userId is missing', async () => {
      const MISSING_NAME_ERROR = 'Room needs an owner'
      const WRONG_USER_HEADER = {}
      const spyCreate = createRoomUseCase.execute as jest.Mock

      await expect(async () =>
        controller.create(WRONG_USER_HEADER as unknown as Headers, CREATE_ROOM_REQUEST as CreateRoomRequest)
      ).rejects.toThrowError(MISSING_NAME_ERROR)
      expect(spyCreate).not.toHaveBeenCalled()
    })
  })

  describe('POST addUserToRoom', () => {
    const ROOM_ID = 'roomId'
    const UPDATE_ROOM_DTO = { userId: 'userId' }
    const ROOMUSERJOIN_CREATED = {}

    it('user should add himself correctly to a room', async () => {
      validateSpy.mockImplementation(() => true)
      const spyCreate = (addUserToRoomUseCase.execute as jest.Mock).mockReturnValue(ROOMUSERJOIN_CREATED)

      const response = await controller.addUserToRoom(USER_HEADER as unknown as Headers, ROOM_ID, UPDATE_ROOM_DTO)

      expect(response).toStrictEqual(ROOMUSERJOIN_CREATED)
      expect(spyCreate).toHaveBeenCalledWith(ROOM_ID, USER_ID, USER_ID)
    })

    it(`room's owner should add another user correctly to the room`, async () => {
      validateSpy.mockImplementation(() => true)
      const spyCreate = (addUserToRoomUseCase.execute as jest.Mock).mockReturnValue(ROOMUSERJOIN_CREATED)

      const response = await controller.addUserToRoom(OWNER_HEADER as unknown as Headers, ROOM_ID, UPDATE_ROOM_DTO)

      expect(response).toStrictEqual(ROOMUSERJOIN_CREATED)
      expect(spyCreate).toHaveBeenCalledWith(ROOM_ID, OWNER_ID, USER_ID)
    })

    it(`should throw error if the updateRoomDto is missing the user id`, async () => {
      const WRONG_UPDATEROOM_DTO = {}

      await expect(async () =>
        controller.addUserToRoom(
          OWNER_HEADER as unknown as Headers,
          ROOM_ID,
          WRONG_UPDATEROOM_DTO as AddUserToRoomRequest
        )
      ).rejects.toThrowError('Missing user id')
    })

    it(`should throw error if the ROOM id has not a valid UUID format`, async () => {
      validateSpy.mockImplementation(() => false)

      await expect(async () =>
        controller.addUserToRoom(OWNER_HEADER as unknown as Headers, ROOM_ID, UPDATE_ROOM_DTO)
      ).rejects.toThrowError(`The room id ${ROOM_ID} is not valid`)
    })

    it(`should throw error if the USER id has not a valid UUID format`, async () => {
      validateSpy.mockImplementationOnce(() => true).mockImplementation(() => false)

      await expect(async () =>
        controller.addUserToRoom(OWNER_HEADER as unknown as Headers, ROOM_ID, UPDATE_ROOM_DTO)
      ).rejects.toThrowError(`The user id ${USER_ID} is not valid`)
    })
  })
})
