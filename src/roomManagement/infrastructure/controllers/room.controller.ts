import { Controller, Post, Body, Param, Headers, BadRequestException } from '@nestjs/common'
import { validate } from 'uuid'

import { AddUserToRoomUseCase } from '../../application/useCases/addUserToRoom.useCase'
import { CreateRoomUseCase } from '../../application/useCases/createRoom.useCase'
import { Room } from '../../domain/entities/room.entity'
import { CreateRoomRequest } from './request/createRoomRequest'
import { AddUserToRoomRequest } from './request/addUserToRoomRequest'
import { ApiCreatedResponse, ApiHeader, ApiTags } from '@nestjs/swagger'
import { RoomUsersJoin } from '../../domain/entities/roomUsersJoin.entity'

@Controller('room_management/rooms')
@ApiTags('Room_management')
export class RoomController {
  constructor(
    private readonly createRoomUseCase: CreateRoomUseCase,
    private readonly addUserToRoomUseCase: AddUserToRoomUseCase
  ) {}

  @Post()
  @ApiHeader({
    name: 'user-id',
    description: 'User identifier'
  })
  @ApiCreatedResponse({ type: Room })
  create(@Headers() headers: Headers, @Body() createRoomDto: CreateRoomRequest) {
    const ownerId = headers['user-id']
    const { name, description } = createRoomDto

    const room = new Room(name, ownerId, description)

    return this.createRoomUseCase.execute(room)
  }

  @Post('/:roomId/users')
  @ApiHeader({
    name: 'user-id',
    description: 'User identifier'
  })
  @ApiCreatedResponse({ type: RoomUsersJoin })
  addUserToRoom(
    @Headers() headers: Headers,
    @Param('roomId') roomId: string,
    @Body() addUserToRoomDto: AddUserToRoomRequest
  ) {
    const applicantId = headers['user-id']
    const { userId } = addUserToRoomDto

    if (!userId) throw new BadRequestException('Missing user id')
    if (!validate(roomId)) throw new BadRequestException(`The room id ${roomId} is not valid`)
    if (!validate(userId)) throw new BadRequestException(`The user id ${userId} is not valid`)

    return this.addUserToRoomUseCase.execute(roomId, applicantId, userId)
  }
}
