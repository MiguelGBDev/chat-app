import { ApiProperty } from '@nestjs/swagger'
export class CreateRoomRequest {
  @ApiProperty()
  name: string

  @ApiProperty({ required: false })
  description?: string
}
