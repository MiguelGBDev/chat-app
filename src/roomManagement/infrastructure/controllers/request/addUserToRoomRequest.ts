import { ApiProperty } from '@nestjs/swagger'

export class AddUserToRoomRequest {
  @ApiProperty()
  userId: string
}
