import { Test, TestingModule } from '@nestjs/testing'
import { PrismaClient } from '@prisma/client'
import { createMock } from '@golevelup/ts-jest'

import { PrismaRoomUserRepository } from './prisma.roomUser.repository'
import { RoomUsersJoin } from '../../domain/entities/roomUsersJoin.entity'

const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const USERROOMJOIN_DB = { roomId: ROOM_ID, userId: USER_ID }
const RETURNED_ROOMUSERJOIN = new RoomUsersJoin(USER_ID, ROOM_ID)

describe('PrismaRoomUserRepository', () => {
  let repository: PrismaRoomUserRepository
  let prismaClientMock: PrismaClient

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrismaRoomUserRepository, { provide: PrismaClient, useValue: createMock<PrismaClient>() }]
    }).compile()

    repository = module.get<PrismaRoomUserRepository>(PrismaRoomUserRepository)
    prismaClientMock = module.get<PrismaClient>(PrismaClient)
  })

  it('should be defined', () => {
    expect(repository).toBeDefined()
  })

  describe('create', () => {
    it('should return the new room', async () => {
      prismaClientMock.roomUsersJoin.create = jest.fn().mockReturnValue(USERROOMJOIN_DB)

      const response = await repository.create(ROOM_ID, USER_ID)

      expect(response).toStrictEqual(RETURNED_ROOMUSERJOIN)
      expect(prismaClientMock.roomUsersJoin.create).toHaveBeenCalled()
    })

    it('should throw error if already exist a row with same user and room', async () => {
      prismaClientMock.roomUsersJoin.create = jest.fn().mockRejectedValue(new Error())

      await expect(async () => repository.create(ROOM_ID, USER_ID)).rejects.toThrowError()
      expect(prismaClientMock.roomUsersJoin.create).toHaveBeenCalled()
    })
  })
})
