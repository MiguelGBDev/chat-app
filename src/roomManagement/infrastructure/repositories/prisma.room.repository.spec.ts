import { Test, TestingModule } from '@nestjs/testing'
import { PrismaClient } from '@prisma/client'
import { createMock } from '@golevelup/ts-jest'

import { PrismaRoomRepository } from './prisma.room.repository'
import { Room } from '../../domain/entities/room.entity'

const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const ROOM_NAME = 'newRoom'
const ROOM = new Room(ROOM_ID, USER_ID)
const ROOM_DB = { id: ROOM_ID, name: ROOM_NAME, ownerId: USER_ID, description: null }
const RETURNED_ROOM = new Room(ROOM_NAME, USER_ID, null, ROOM_ID)

describe('PrismaRoomRepository', () => {
  let repository: PrismaRoomRepository
  let prismaClientMock: PrismaClient

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrismaRoomRepository, { provide: PrismaClient, useValue: createMock<PrismaClient>() }]
    }).compile()

    repository = module.get<PrismaRoomRepository>(PrismaRoomRepository)
    prismaClientMock = module.get<PrismaClient>(PrismaClient)
  })

  it('should be defined', () => {
    expect(repository).toBeDefined()
  })

  describe('create', () => {
    it('should return the new room', async () => {
      prismaClientMock.room.create = jest.fn().mockReturnValue(RETURNED_ROOM)
      prismaClientMock.roomUsersJoin.create = jest.fn().mockReturnValue({ ROOM_ID, USER_ID })

      const response = await repository.create(ROOM)

      expect(response).toStrictEqual(RETURNED_ROOM)
      expect(prismaClientMock.room.create).toHaveBeenCalled()
      expect(prismaClientMock.roomUsersJoin.create).toHaveBeenCalled()
    })

    it('should throw error if already exist a room with the same name', async () => {
      prismaClientMock.room.create = jest.fn().mockRejectedValue(new Error())

      await expect(async () => repository.create(ROOM)).rejects.toThrowError()
      expect(prismaClientMock.room.create).toHaveBeenCalled()
    })
  })

  describe('getById', () => {
    it('should return the room', async () => {
      prismaClientMock.room.findUnique = jest.fn().mockReturnValue(ROOM_DB)

      const response = await repository.getById(USER_ID)

      expect(response).toStrictEqual(RETURNED_ROOM)
      expect(prismaClientMock.room.findUnique).toHaveBeenCalled()
    })

    it('should return undefined if the room is not found', async () => {
      prismaClientMock.room.findUnique = jest.fn().mockReturnValue(undefined)

      const response = await repository.getById(RETURNED_ROOM.id)

      expect(response).toStrictEqual(undefined)
      expect(prismaClientMock.room.findUnique).toHaveBeenCalled()
    })
  })

  describe('getByName', () => {
    it('should return the room', async () => {
      prismaClientMock.room.findUnique = jest.fn().mockReturnValue(ROOM_DB)

      const response = await repository.getByName(ROOM_NAME)

      expect(response).toStrictEqual(RETURNED_ROOM)
      expect(prismaClientMock.room.findUnique).toHaveBeenCalled()
    })

    it('should return undefined if the room is not found', async () => {
      prismaClientMock.room.findUnique = jest.fn().mockReturnValue(undefined)

      const response = await repository.getByName(ROOM_NAME)

      expect(response).toStrictEqual(undefined)
      expect(prismaClientMock.room.findUnique).toHaveBeenCalled()
    })
  })
})
