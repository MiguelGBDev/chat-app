import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { RoomUsersJoin } from '../../domain/entities/roomUsersJoin.entity'
import { RoomUserRepository } from '../../domain/repositories/RoomUserRepository'

@Injectable()
export class PrismaRoomUserRepository implements RoomUserRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async create(roomId: string, userId: string): Promise<RoomUsersJoin> {
    const response = await this.prisma.roomUsersJoin.create({
      data: {
        roomId,
        userId
      }
    })

    return new RoomUsersJoin(response.userId, response.roomId)
  }
}
