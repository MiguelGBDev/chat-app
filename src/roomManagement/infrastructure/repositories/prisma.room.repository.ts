import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { Room } from '../../domain/entities/room.entity'
import { RoomRepository } from '../../domain/repositories/RoomRepository'

@Injectable()
export class PrismaRoomRepository implements RoomRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async create(room: Room): Promise<Room> {
    const response = await this.prisma.room.create({
      data: {
        name: room.name,
        description: room.description,
        ownerId: room.ownerId
      }
    })
    await this.prisma.roomUsersJoin.create({ data: { roomId: response.id, userId: room.ownerId } })

    return response ? new Room(response.name, response.ownerId, response.description, response.id) : undefined
  }

  async getById(roomId: string): Promise<Room> {
    const response = await this.prisma.room.findUnique({ where: { id: roomId } })
    return response ? new Room(response.name, response.ownerId, response.description, response.id) : undefined
  }

  async getByName(name: string): Promise<Room> {
    const response = await this.prisma.room.findUnique({ where: { name } })
    return response ? new Room(response.name, response.ownerId, response.description, response.id) : undefined
  }
}
