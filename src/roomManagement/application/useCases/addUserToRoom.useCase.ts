import { ForbiddenException, Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common'

import { RoomUsersJoin } from '../../../roomManagement/domain/entities/roomUsersJoin.entity'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { RoomUserRepository } from '../../domain/repositories/RoomUserRepository'
import { UserRepository } from '../../domain/repositories/UserRepository'

@Injectable()
export class AddUserToRoomUseCase {
  constructor(
    private readonly roomRepository: RoomRepository,
    private readonly userRepository: UserRepository,
    private readonly roomUserRepository: RoomUserRepository
  ) {}

  async execute(roomId: string, applicantId: string, userId: string): Promise<RoomUsersJoin> {
    const room = await this.roomRepository.getById(roomId)
    if (!room) throw new NotFoundException(`Room with id ${roomId} not found`)

    const [applicant, user] = await Promise.all([
      this.userRepository.getById(applicantId),
      this.userRepository.getById(userId)
    ])

    if (!applicant) throw new NotFoundException(`User with id ${applicantId} not found`)
    if (!user) throw new NotFoundException(`User with id ${userId} not found`)

    if (applicantId !== userId && applicantId !== room.ownerId)
      throw new ForbiddenException('The user has no privileges to this action')

    if (user.rooms?.includes(roomId)) throw new NotAcceptableException('The user is already on the room')

    return await this.roomUserRepository.create(roomId, userId)
  }
}
