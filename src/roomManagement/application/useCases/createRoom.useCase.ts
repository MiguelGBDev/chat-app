import { Injectable, NotAcceptableException } from '@nestjs/common'

import { Room } from '../../../roomManagement/domain/entities/room.entity'
import { RoomRepository } from '../../domain/repositories/RoomRepository'

@Injectable()
export class CreateRoomUseCase {
  constructor(private readonly roomRepository: RoomRepository) {}

  async execute(room: Room): Promise<Room> {
    const roomDb = await this.roomRepository.getByName(room.name)
    if (roomDb) throw new NotAcceptableException(`The room name '${room.name}' is already used`)

    return await this.roomRepository.create(room)
  }
}
