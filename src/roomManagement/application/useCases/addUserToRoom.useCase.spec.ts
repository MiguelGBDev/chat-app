import { Test, TestingModule } from '@nestjs/testing'
import { createMock } from '@golevelup/ts-jest'

import { AddUserToRoomUseCase } from './addUserToRoom.useCase'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { RoomUserRepository } from '../../domain/repositories/RoomUserRepository'
import { UserRepository } from '../../domain/repositories/UserRepository'
import { User } from '../../domain/entities/user.entity'

const USER_ID = 'userId'
const ROOM_ID = 'roomId'
const OWNER_ID = 'ownerId'
const ROOM_DB = { ownerId: OWNER_ID }

describe('AddUserToRoomUseCase', () => {
  let useCase: AddUserToRoomUseCase
  let roomRepository: RoomRepository
  let userRepository: UserRepository
  let roomUserRepository: RoomUserRepository

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AddUserToRoomUseCase,
        { provide: RoomRepository, useValue: createMock<RoomRepository>() },
        { provide: UserRepository, useValue: createMock<UserRepository>() },
        { provide: RoomUserRepository, useValue: createMock<RoomUserRepository>() }
      ]
    }).compile()

    useCase = module.get<AddUserToRoomUseCase>(AddUserToRoomUseCase)
    roomRepository = module.get<RoomRepository>(RoomRepository)
    userRepository = module.get<UserRepository>(UserRepository)
    roomUserRepository = module.get<RoomUserRepository>(RoomUserRepository)
  })

  it('should be defined', () => {
    expect(useCase).toBeDefined()
  })

  describe('execute', () => {
    const OWNER_DB = {}
    const USER_DB = {}
    const ROOMUSER_DB = {}

    it(`user should add himself correctly to the room`, async () => {
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(ROOM_DB)
      const spyUserGetById = (userRepository.getById as jest.Mock).mockReturnValue(USER_DB)
      const spyRoomUserCreate = (roomUserRepository.create as jest.Mock).mockReturnValue(ROOMUSER_DB)

      const response = await useCase.execute(ROOM_ID, USER_ID, USER_ID)
      expect(response).toBe(ROOMUSER_DB)
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(1, USER_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(2, USER_ID)
      expect(spyRoomUserCreate).toHaveBeenCalledWith(ROOM_ID, USER_ID)
    })

    it(`room's owner should add another user correctly to the room`, async () => {
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(ROOM_DB)
      const spyUserGetById = (userRepository.getById as jest.Mock)
        .mockReturnValueOnce(OWNER_DB)
        .mockReturnValue(USER_DB)
      const spyRoomUserCreate = (roomUserRepository.create as jest.Mock).mockReturnValue(ROOMUSER_DB)

      const response = await useCase.execute(ROOM_ID, OWNER_ID, USER_ID)
      expect(response).toBe(ROOMUSER_DB)
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(1, OWNER_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(2, USER_ID)
      expect(spyRoomUserCreate).toHaveBeenCalledWith(ROOM_ID, USER_ID)
    })

    it(`should throw error if room is not found`, async () => {
      const WRONG_ROOM_ID = 'wrongRoomId'
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(null)
      const spyUserGetById = userRepository.getById as jest.Mock
      const spyRoomUserCreate = roomUserRepository.create as jest.Mock

      await expect(useCase.execute(WRONG_ROOM_ID, OWNER_ID, USER_ID)).rejects.toThrowError(
        `Room with id ${WRONG_ROOM_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(WRONG_ROOM_ID)
      expect(spyUserGetById).not.toHaveBeenCalled()
      expect(spyRoomUserCreate).not.toHaveBeenCalled()
    })

    it(`should throw error if applicant is not found`, async () => {
      const WRONG_APPLICANT_ID = 'wrongUserId'
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(ROOM_DB)
      const spyUserGetById = (userRepository.getById as jest.Mock).mockReturnValueOnce(null).mockReturnValue(USER_DB)
      const spyRoomUserCreate = roomUserRepository.create as jest.Mock

      await expect(useCase.execute(ROOM_ID, WRONG_APPLICANT_ID, USER_ID)).rejects.toThrowError(
        `User with id ${WRONG_APPLICANT_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(1, WRONG_APPLICANT_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(2, USER_ID)
      expect(spyRoomUserCreate).not.toHaveBeenCalled()
    })

    it(`should throw error if the user to add is not found`, async () => {
      const WRONG_USER_ID = 'wrongUserId'
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(ROOM_DB)
      const spyUserGetById = (userRepository.getById as jest.Mock).mockReturnValueOnce(OWNER_DB).mockReturnValue(null)
      const spyRoomUserCreate = roomUserRepository.create as jest.Mock

      await expect(useCase.execute(ROOM_ID, OWNER_ID, WRONG_USER_ID)).rejects.toThrowError(
        `User with id ${WRONG_USER_ID} not found`
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(1, OWNER_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(2, WRONG_USER_ID)
      expect(spyRoomUserCreate).not.toHaveBeenCalled()
    })

    it(`random user should not be allowed to add another user to a room not owned by him`, async () => {
      const RANDOM_USER_ID = 'wrongUserId'
      const RANDOM_USER_DB = {}
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(ROOM_DB)
      const spyUserGetById = (userRepository.getById as jest.Mock)
        .mockReturnValueOnce(RANDOM_USER_DB)
        .mockReturnValue(USER_DB)
      const spyRoomUserCreate = roomUserRepository.create as jest.Mock

      await expect(useCase.execute(ROOM_ID, RANDOM_USER_ID, USER_ID)).rejects.toThrowError(
        'The user has no privileges to this action'
      )
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(1, RANDOM_USER_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(2, USER_ID)
      expect(spyRoomUserCreate).not.toHaveBeenCalled()
    })

    it(`should throw an error if the user is already on the room`, async () => {
      const USER_WITH_ROOM = new User(USER_ID, [ROOM_ID])
      const spyRoomGetById = (roomRepository.getById as jest.Mock).mockReturnValue(ROOM_DB)
      const spyUserGetById = (userRepository.getById as jest.Mock).mockReturnValue(USER_WITH_ROOM)
      const spyRoomUserCreate = roomUserRepository.create as jest.Mock

      await expect(useCase.execute(ROOM_ID, USER_ID, USER_ID)).rejects.toThrowError('The user is already on the room')
      expect(spyRoomGetById).toHaveBeenCalledWith(ROOM_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(1, USER_ID)
      expect(spyUserGetById).toHaveBeenNthCalledWith(2, USER_ID)
      expect(spyRoomUserCreate).not.toHaveBeenCalled()
    })
  })
})
