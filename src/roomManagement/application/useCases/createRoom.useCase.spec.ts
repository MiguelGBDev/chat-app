import { Test, TestingModule } from '@nestjs/testing'
import { createMock } from '@golevelup/ts-jest'

import { CreateRoomUseCase } from './createRoom.useCase'
import { RoomRepository } from '../../domain/repositories/RoomRepository'
import { Room } from '../../domain/entities/room.entity'

const USER_ID = 'userId'
const OWNER_ID = 'ownerId'
const ROOM_DB = { ownerId: OWNER_ID }

describe('CreateRoomUseCase', () => {
  let useCase: CreateRoomUseCase
  let roomRepository: RoomRepository

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CreateRoomUseCase, { provide: RoomRepository, useValue: createMock<RoomRepository>() }]
    }).compile()

    useCase = module.get<CreateRoomUseCase>(CreateRoomUseCase)
    roomRepository = module.get<RoomRepository>(RoomRepository)
  })

  it('should be defined', () => {
    expect(useCase).toBeDefined()
  })

  describe('create', () => {
    const ROOM_NAME = 'roomName'
    const ROOM = new Room(ROOM_NAME, USER_ID)

    it('should return the new room', async () => {
      const spyRoomGetNameId = (roomRepository.getByName as jest.Mock).mockReturnValue(null)
      const spyCreate = (roomRepository.create as jest.Mock).mockReturnValue(ROOM)

      const response = await useCase.execute(ROOM)

      expect(response).toStrictEqual(ROOM)
      expect(spyRoomGetNameId).toHaveBeenCalledWith(ROOM.name)
      expect(spyCreate).toHaveBeenCalledWith(ROOM)
    })

    it('should throw error if already exist a room with the same name', async () => {
      const spyRoomGetNameId = (roomRepository.getByName as jest.Mock).mockReturnValue(ROOM_DB)
      const spyCreate = roomRepository.create as jest.Mock

      await expect(useCase.execute(ROOM)).rejects.toThrowError(`The room name '${ROOM_NAME}' is already used`)
      expect(spyRoomGetNameId).toHaveBeenCalledWith(ROOM.name)
      expect(spyCreate).not.toHaveBeenCalled()
    })
  })
})
